Consider the following elements:
Sodium, Sulphur, Carbon, Magnesium
Which of these elements will form:

(a) acidic oxides.
(a) Carbon and Sulphur will form acidic oxides. Nonmetals react with oxygen to form nonmetallic oxides. These oxides differ from metallic oxides because they are acidic in nature.
Therefore, oxides of sulphur and carbon i.e. sulphur dioxide and carbon dioxide are acidic in nature.

(b) Basic oxides.
(b) Sodium and magnesium will form basic oxides. Metals react with oxygen to form metallic oxides. These metallic oxides are basic because they react with water to form bases.

Classify the following into acidic oxides and basic oxides:

Na2O, SO2, MgO, CO2

What is the nature (acidic/basic) of the following oxides?

(a) Magnesium oxide
(b) Sulphur dioxide

(a) Magnesium is a metal. Metals react with oxygen to form metallic oxides. Hence, Magnesium oxides are basic in nature.
(b) Sulphur dioxide is a nonmetal. Nonmetals react with oxygen to form nonmetallic oxides. These oxides differ from metallic oxides because they are acidic in nature.

(a)What are amphoteric oxides? Give two examples of amphoteric oxides.

(b)Choose the acidic oxides, basic oxides and neutral oxides from the following:

Na2O; CO2.; CO; SO2; MgO; N2O; H2O

(c)Which of the following are amphoteric oxides:

Mgo, ZnO, P2O3, Al2O3, NO2

(a) Amphoteric oxides are those oxides that are basic as well as acidic character.
Examples: Aluminium oxide (Al2O3) and zinc oxide (ZnO).
(b) Acidic oxide: CO2; CO; SO2
Basic oxides: Na2O; MgO;
Neutral oxide: H2O
(c) ZnO, Al2O3 are amphoteric oxides.

Give the names and formulae of (a) two acidic oxides, and (b) two basic oxides.

(a) Carbon dioxide (CO2) and sulphur dioxide (SO2) are the two acidic oxides.
(b) Sodium oxide (Na2O) and magnesium oxides (MgO) are two basic oxides.


Define 
(i) Ionisation 
(ii) Neutralisation 
(iii) Dilution 
(iv) Salt formation 

What is a displacement reaction?
Displacement reaction is a chemical reaction in which a more reactive element displaces a less reactive element from its compound. Both metals and non-metals take part in displacement reactions.

Example : Reaction of iron nails with copper sulphate solution.

The process of reduction involves, 
Reduction involves-
Removal of O2  /E.N element.
Addition of H/electropositive element.
Gain of electrons.
Decrease in oxidation number.

When substance undergoes reduction there will be gain of electrons and decrease in oxidation number. As substance gained electrons so there will be decrease in valency of electropositive element.
But during reduction substance never undergoes loss of electrons.

What is sublimation
Sublimation is the process of conversion of substance from solid phase directly to gaseous phase, there is no intermediate phase
Ex:Comphor in air and napthalene balls in air.

Sublimation is a specialized change of state when a solid substance skips the liquid phase and moves directly into the gas phase. This occurs because the substance absorbs energy so quickly from the surroundings that melting never occurs.
Examples:
 "Dry ice" or solid carbon dioxide sublimes.
 Snow and ice can sublime in the winter months without melting.

volatile substances are those substances which convert into the form of vapour from solid and liquid. 
non volatile substance are those substances which do not convert in the form of vapour from solid and liquid.
Sublimation is the process by which a solid directly converts into gas without undergoing liquid state or vice versa. 
For example, naphthalene.

Manganese dioxide, MnO2

What is Sublimation of dry ice

double displacement reaction
Those reactions in which two compounds react by an exchange of ions to form two new compounds are called double displacement reactions. In double replacement reactions, the positive ions exchange negative ion partners. Many double displacement reactions occur between ionic compounds that are dissolved in water. A double replacement reaction is represented by the general equation

Lead acetate can be used in place of lead nitrate because lead acetate is also water soluble like lead nitrate. When potassium iodide and lead acetate are mixed, a bright yellow precipitate of lead iodide is seen. Lead iodide is unusually soluble in hot water and forms crystals on cooling.



